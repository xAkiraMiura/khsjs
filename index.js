javascript:(
    function() {
        try {
            new Function();
        } catch (e) {
            alert("This site has CSP enabled and scripts cannot be injected.");
            return;
        }
        
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status != 200) {
                    alert(`HTTP Error ${this.status}`);
                } else {
                    new Function(atob(JSON.parse(xhr.responseText).content))();
                }
            }
        };

        xhr.open("GET", "https://gitlab.com/api/v4/projects/18393394/repository/files/bootstrap.js?ref=master", true);
        xhr.send();
    }()
);
