class Modules {
    modules;

    constructor() {
        if (KHsJS.modules && KHsJS.modules.Unload)
            KHsJS.modules.Unload();

        this.modules = [];

        /* temp until better module loader */
        this.LoadModule("https://gitlab.com/api/v4/projects/18393394/repository/files/example_module.js?ref=master");
        this.LoadModule("https://gitlab.com/api/v4/projects/18393394/repository/files/example_module2.js?ref=master");
    }

    async LoadModule (path) {
        let module = new new new Function(atob(JSON.parse(await KHsJS.GetURL(path)).content))
        if (!module) return;
        this.modules.push(module);
        if (module.LoadInit) module.LoadInit();
        if (KHsJS.menu && KHsJS.menu.isReady && module.MenuInit) module.MenuInit(); 
    }

    Unload() {
        for (let i = 0; i < this.modules.length; i++) {
            if (this.modules[i].Unload) this.modules[i].Unload();
            this.modules[i] = undefined;
        }
        this.modules = undefined;
    }
}

KHsJS.modules = new Modules();
