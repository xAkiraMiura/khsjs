class KHsMenu {
    isReady = false;
    ids = [];
    object;

    constructor() {
        if (KHsJS.menu && KHsJS.menu.Unload)
            KHsJS.menu.Unload();

        this.Setup().then(() => {
            this.OnReady();
        });
    }

    OnReady() {
        this.isReady = true;
        this.CreateButton('Unload', KHsJS.Unload);
        if (KHsJS.modules && KHsJS.modules.modules) {
            for (let i = 0; i < KHsJS.modules.modules.length; i++) {
                KHsJS.modules.modules[i].MenuInit();
            }
        }
    }

    async Setup() {
        this.style = document.createElement("style");
        this.style.textContent = await KHsJS.GetDataInternal("menu.css");
        document.body.appendChild(this.style);

        this.object = document.createElement("div");
        this.object.id = "KHsMenu";
        document.body.append(this.object);

        let background = document.createElement('div');
        background.id = "KHsMenuBackground";
        background.className = "KHsFull";
        this.object.appendChild(background);

        let titlebar = document.createElement('div');
        titlebar.id = "KHsTitle";
        titlebar.className = "KHsSharpen";
        titlebar.textContent = "KHs.JS";
        this.object.appendChild(titlebar);
    }

    Toggle() {
        if (!this.object.style.display) this.object.style.display = "initial";
        else this.object.style.display = this.object.style.display == "none" ? "initial" : "none";
    }

    Unload() {
        this.object.parentNode.removeChild(this.object);
        this.style.parentNode.removeChild(this.style);
    }

    GenerateUnique() {
        let id;
        do {
            id = Math.floor(Math.random() * 16777216);
        } while (this.ids.includes(id));
        this.ids.push(id);
        return id;
    }

    Alloc() {
        let element = document.createElement('div');
        element.id = `KHsJS-${this.GenerateUnique()}`;
        return element;
    }

    Release(id) {
        let element = document.getElementById(`KHsJS-${id}`);
        if (!element) return false;
        element.parentNode.removeChild(element);
        return true;
    }

    CreateButton(text, callback) {
        var button = this.Alloc();
        button.className = "KHsButton KHsSharpen";
        button.onclick = callback;
        button.textContent = text;

        this.object.appendChild(button);
    }
}

KHsJS.menu = new KHsMenu();
