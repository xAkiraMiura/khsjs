class TitleHider {
    oHook;

    isActive = false;
    oName = "";

    /* called by module loader. LoadInit, MenuInit, Unload */
    LoadInit() {
        this.oHook = document.onvisibilitychange;
        document.onvisibilitychange = () => {
            if (this.isActive) {
                if (document.hidden) {
                    this.oName = document.title;
                    document.title = "<hidden>";
                } else if (!document.hidden) {
                    document.title = this.oName;
                }
            }
            this.oHook();
        }
    }

    MenuInit() {
        KHsJS.menu.CreateButton("Hide Title", () => {this.isActive ^= 1});
    }

    Unload() {
        document.onvisibilitychange = this.oHook;
    }
}

return TitleHider;
