class HighContract {
    styleSheet;
    isActive = false;

    /* called by module loader, LoadInit, MenuInit, Unload */
    LoadInit() {
        this.styleSheet = document.createElement("style");
        this.styleSheet.textContent = "* { background-color: black !important; color: white !important }";
    }
    MenuInit() {
        KHsJS.menu.CreateButton("High Contrast", () => {this.Toggle()});
    }
    Unload() {
        this.Disable();
    }

    /* custom functions */
    Toggle() {
        if (this.isActive) this.Disable(); else this.Enable();
    }
    Enable() {
        this.isActive = true;
        document.body.appendChild(this.styleSheet);
    }
    Disable() {
        this.isActive = false;
        document.body.removeChild(this.styleSheet);
    }
}

return HighContract;
