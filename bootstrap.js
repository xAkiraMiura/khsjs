if (window.KHsJS == undefined) {
    window.KHsJS = {};
    KHsJS.GetURL = (url) => {
        return new Promise((res, rej) => {
            let xhr = new XMLHttpRequest();
        
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    if (xhr.status != 200) {
                        rej(xhr.status);
                    } else {
                        res(xhr.responseText);
                    }
                }
            };

            xhr.open("GET", url, true);
            xhr.send();
        });
    };
    KHsJS.GetDataInternal = async (path) => {
        return atob(JSON.parse(await KHsJS.GetURL(`https://gitlab.com/api/v4/projects/18393394/repository/files/${path}?ref=master`)).content);
    };
    KHsJS.LoadModuleInternal = async (path) => {
        new Function(await KHsJS.GetDataInternal(path))();
    };
} else {
    if (KHsJS.injected) {
        alert("KHsJS is already injected.");
        return;
    }
}
KHsJS.injected = true;

KHsJS.LoadModuleInternal("core.js");
KHsJS.LoadModuleInternal("modules.js");
KHsJS.LoadModuleInternal("menu.js");
