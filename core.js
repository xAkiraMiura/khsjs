alert("Successfully loaded.");

KHsJS.oKeyDown = document.body.onkeydown;
document.body.onkeydown = (e) => {
    switch (e.key) {
        case "Shift":
            KHsJS.isShift = true;
            break;
        case "End":
            e.preventDefault();
            KHsJS.Unload();
            break;
        case "Insert":
            if (KHsJS.menu && KHsJS.menu.Toggle)
                KHsJS.menu.Toggle();
            break;
    }
    
    if (KHsJS.oKeyDown != null && KHsJS.oKeyDown != undefined) {
        KHsJS.oKeyDown();
    }
}

KHsJS.oKeyUp = document.body.onkeyup;
document.body.onkeyup = (e) => {
    if (e.key == 'Shift') {
        KHsJS.isShift = false;
    }
    
    if (KHsJS.oKeyUp != null && KHsJS.oKeyUp != undefined) {
        KHsJS.oKeyUp();
    }
}

KHsJS.Unload = function () {
    document.body.onkeydown = KHsJS.oKeyDown;
    document.body.onkeyup = KHsJS.oKeyUp;

    if (KHsJS.menu) {
        if (KHsJS.menu.Unload) KHsJS.menu.Unload();
        delete KHsJS.menu;
    }

    if (KHsJS.modules) {
        if (KHsJS.modules.Unload) KHsJS.modules.Unload();
        delete KHsJS.menu;
    }
    
    KHsJS.injected = false;
    
    if (KHsJS.isShift) delete window.KHsJS;
}
